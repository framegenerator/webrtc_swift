//
//  CallVC.swift
//  Chatroom
//
//  Created by Golovanov, Eugene on 7/31/17.
//  Copyright © 2017 eugene golovanov. All rights reserved.
//

import UIKit
import AVFoundation
import WebRTC


protocol CallVCDelegate: NSObjectProtocol {
    func viewControllerDidFinish(_ viewController: CallVC)
}

class CallVC: UIViewController {
    
    weak var delegate: CallVCDelegate?

    private var _remoteVideoTrack: RTCVideoTrack?
    var remoteVideoTrack: RTCVideoTrack? {
        get {
            return _remoteVideoTrack
        }
        set(remoteVideoTrack) {
            if _remoteVideoTrack == remoteVideoTrack {
                return
            }
            _remoteVideoTrack?.remove(videoCallView.remoteVideoView)
            _remoteVideoTrack = nil
            videoCallView.remoteVideoView.renderFrame(nil)
            _remoteVideoTrack = remoteVideoTrack
            _remoteVideoTrack?.add(videoCallView.remoteVideoView)
        }
    }
    
    private(set) var videoCallView: VideoCallView = VideoCallView(frame: CGRect.zero)
    
    var client:AppClient?
    var captureController: ARDCaptureController!
    var portOverride: AVAudioSessionPortOverride = .none
    
    
    // MARK: - Init
    
    //TODO: Handle isAudioOnly
    convenience init(isInitiator: Bool, isAudioOnly: Bool, delegate: CallVCDelegate) {
        self.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        
        //Connect
        if let clnt = WebRTCManager.shared.appClient {
            self.client = clnt
            clnt.delegate = self
        } else {
            self.client = AppClient.init(delegate: self)
            WebRTCManager.shared.appClient = self.client
        }
        self.client?.isInitiator = isInitiator
        self.client?.startSignalingIfReady()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        print("\n===========================   CallVC Deinit    ===========================\n")
    }
    
    // MARK: - View Lifecycle
    
    override func loadView() {
        super.loadView()
//        self.videoCallView = VideoCallView(frame: CGRect.zero)
        self.videoCallView.delegate = self
//        self.videoCallView.statusLabel.text = statusText(for: .new)
        self.view = videoCallView
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
//        remoteVideoView.frame = CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: 0, height: 0)
//        remoteVideoView.delegate = self
//        self.view.addSubview(remoteVideoView)
//        
//        let locRect = CGRect(x: 10, y: 10, width: self.view.frame.width/2, height: self.view.frame.height/2)
//        localVideoView.frame = locRect
//        self.view.addSubview(localVideoView)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}

extension CallVC: AppClientDelegate {
    
    func appClient(client:AppClient, didReceiveLocal videoTrack:RTCVideoTrack) {
    }
    
    func appClient(client:AppClient, didReceiveRemote videoTrack:RTCVideoTrack) {
        print("-==-->didReceiveRemote videoTrack: \(videoTrack)")
        self.remoteVideoTrack = videoTrack
    }
    
    func appClient(_ client: AppClient, didCreateLocalCapturer localCapturer: RTCCameraVideoCapturer) {
        print("-=-->didCreateLocalCapturer: \(localCapturer)")
//        self.localVideoView.captureSession = localCapturer.captureSession
//        self.captureController = ARDCaptureController(capturer: localCapturer)
//        self.captureController.startCapture()
        
        
        self.videoCallView.localVideoView.captureSession = localCapturer.captureSession
        self.captureController = ARDCaptureController(capturer: localCapturer)
        self.captureController.startCapture()
    }
    
    func appClient(_ client: AppClient, didError error: Error?) {
        let message: String = "\(error!.localizedDescription)"
        hangup()
        showAlert(withMessage: message)
    }
    
    func appClient(_ client: AppClient, didChange state: AppClientState) {
        switch state {
        case .connected:
            print("Client connected.")
        case .connecting:
            print("Client connecting.")
        case .disconnected:
            print("Client disconnected.")
            hangup()
        }
    }


}

extension CallVC : VideoCallViewDelegate {
    
    // MARK: - VideoCallViewDelegate
    func videoCallViewDidHangup(_ view: VideoCallView) {
        hangup()
    }
    
    func videoCallViewDidSwitchCamera(_ view: VideoCallView) {
        // TODO(tkchin): Rate limit this so you can't tap continously on it.
        // Probably through an animation.
        self.captureController.switchCamera()
    }
    
    func videoCallViewDidChangeRoute(_ view: VideoCallView) {
        var override: AVAudioSessionPortOverride = .none
        if portOverride == .none {
            override = .speaker
        }
    }
    
    // MARK: - Private
    
    func hangup() {
        self.remoteVideoTrack = nil
        self.videoCallView.localVideoView.captureSession = nil
        self.captureController.stopCapture()
        self.captureController = nil
        self.client?.disconnect()
        self.client = nil
        delegate?.viewControllerDidFinish(self)
        WebRTCManager.shared.remoteSDP = nil
        SocketIOManager.sharedInstance.sendBye()
    }
    
    func showAlert(withMessage message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(defaultAction)
        present(alert, animated: true, completion: { _ in })
    }

}

