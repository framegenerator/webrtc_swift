//
//  ChatVC.swift
//  Chatroom
//
//  Created by eugene golovanov on 7/26/16.
//  Copyright © 2016 eugene golovanov. All rights reserved.
//

import UIKit

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource {


    //--------------------------------------------------
    // MARK: - Properties
    
    @IBOutlet weak var tableViewChat: UITableView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
    
    @IBOutlet weak var messageTextField: UITextField!
    
    @IBOutlet weak var buttonCall: UIButton!

    
    var messagesArray = Array<Message>()
    //----------------------------------------------------
    // MARK: - View Lifecycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonCall.isHidden = true
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChatVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        tableViewChat.delegate = self
        tableViewChat.dataSource = self
        tableViewChat.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "chatCell")

        

//        self.fakeData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SocketIOManager.sharedInstance.connectToChatroomWithNickname(chatroom: self.roomLabel.text!, nickname: self.nameLabel.text!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
                
        //RECEIVING MESSAGES
        SocketIOManager.sharedInstance.getChatMessage { (messageInfo) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                print("----------------------MESSAGE INFO:-------------------------------")
                let message = Message(data: messageInfo)
                self.messagesArray.append(message)
                self.tableViewChat.reloadData()
                self.tableViewScrollToBottom(tableView: self.tableViewChat, animated: true)
                print("Received Message: \(message)")
                print("-----------------------------------------------------")                
            })
        }
    }

    //---------------------------------------------------------------------------------------------------------
    //MARK: - init deinit
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.registerWebrtcNotifications()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //-----------------------------------------------------
    // MARK: - Table view scroll animated

    
    func tableViewScrollToBottom(tableView:UITableView, animated: Bool) {
        
        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: time, execute: {
            
            let numberOfSections = tableView.numberOfSections
            let numberOfRows = tableView.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: animated)
            }
            
        })
    }

    
    //-----------------------------------------------------
    // MARK: - Actions

    
    @IBAction func sendMessageAction(_ sender: UIButton) {
        if self.messageTextField.text!.characters.count > 0 {
            
            let message = self.messageTextField.text!
            let name = self.nameLabel.text!
            let timestamp = Int(Date().timeIntervalSince1970 * 1000)
            
            SocketIOManager.sharedInstance.sendMessage(message: message, name: name, timestamp: timestamp)
            
            self.messageTextField.text = ""

        } else {
           let alert = UIAlertController(title: "Empty", message: "Type something to send", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (act) in }))
            present(alert, animated: true, completion: nil)
        }
        
    }

    
    @IBAction func dismissAction(_ sender: UIButton) {
        SocketIOManager.sharedInstance.closeConnection()

        self.dismiss(animated: true, completion: nil)
    }
    
    //-----------------------------------------------------
    // MARK: - End editing

    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    //-------------------------------------------------------
    // MARK: - Fake Data

//    func fakeData() {
//        let data1 = ["text": "bla bla", "name": "julia", "timestamp": 1469681882176] as [String : Any]
//        let mess1 = Message(data: data1 as [String : AnyObject])
//        
//        let data2 = ["text": "How are you?", "name": "eugene", "timestamp": 1469681897263] as [String : Any]
//        let mess2 = Message(data: data2 as [String : AnyObject])
//        
//        let data3 = ["text": "ochen horosho", "name": "julia", "timestamp": 1469681899487] as [String : Any]
//        let mess3 = Message(data: data3 as [String : AnyObject])
//        
//        let data4 = ["text": "tra ta ta", "name": "eugene", "timestamp": 1469681903863] as [String : Any]
//        let mess4 = Message(data: data4 as [String : AnyObject])
//        
//        let data5 = ["text": "Hello", "name": "julia", "timestamp": 1469681906417] as [String : Any]
//        let mess5 = Message(data: data5 as [String : AnyObject])
//        
//        self.messagesArray.append(mess1)
//        self.messagesArray.append(mess2)
//        self.messagesArray.append(mess3)
//        self.messagesArray.append(mess4)
//        self.messagesArray.append(mess5)
//        
//    }

    
    
    
    //------------------------------------------------------
    // MARK: - -UITableView Delegate and Datasource Methods-
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatCell
        let message = self.messagesArray[indexPath.row]
        
        if message.name == self.nameLabel.text {
            cell.lblChatMessage.textAlignment = NSTextAlignment.right
            cell.lblMessageDetails.textAlignment = NSTextAlignment.right
        }
        cell.lblChatMessage.text = message.text
        cell.lblMessageDetails.text = "by \(message.name.uppercased()) @ \(message.timeString)"
        cell.lblChatMessage.textColor = UIColor.darkGray
        
        return cell
        
    }

    
    
    //--------------------------------------------------
    // MARK: - Webrtc
    
    func registerWebrtcNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.enableCallButton), name: NSNotification.Name(rawValue: "showCallButton"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.webrtcOfferReceived), name: NSNotification.Name(rawValue: "webrtcOfferReceived"), object: nil)
    }
    
    func enableCallButton() {
        self.buttonCall.isHidden = false
    }
    
    
    @IBAction func onCallButton(_ sender: UIButton) {
        webrtcStart(isInitiator: true)
    }
    
    private func webrtcStart(isInitiator:Bool) {
        let vc = CallVC(isInitiator: isInitiator, isAudioOnly: false, delegate: self)
//        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true) { 
            //
        }
    }
    
    // MARK: - Offer
    
    func webrtcOfferReceived(_ notification: Notification) {
        
        if let userInfo = (notification as NSNotification).userInfo, let type = userInfo["type"] as? String, let sdp = userInfo["sdp"] as? String {
            print("Offer TYPE: \(type)")
            print("Offer SDP: \(sdp)")
            WebRTCManager.shared.remoteSDP = sdp
            webrtcStart(isInitiator: false)
        }
    }
}

extension ChatVC : CallVCDelegate {
    
    func viewControllerDidFinish(_ viewController: CallVC) {
        if !viewController.isBeingDismissed {
            print("-- Delegation, Dismissing CallVC")
            viewController.dismiss(animated: true, completion: {() -> Void in
//                self.restartAudioPlayerIfNeeded()
            })
        }
    }

}
