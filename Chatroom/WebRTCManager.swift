//
//  WebRTCManager.swift
//  Chatroom
//
//  Created by Golovanov, Eugene on 8/26/17.
//  Copyright © 2017 eugene golovanov. All rights reserved.
//

import Foundation
import WebRTC

class WebRTCManager: NSObject {
    
    static let shared = WebRTCManager()
    
    var appClient : AppClient?
    
    var remoteSDP: String?
    
}
