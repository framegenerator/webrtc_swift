//
//  JoinVC.swift
//  Chatroom
//
//  Created by eugene golovanov on 7/25/16.
//  Copyright © 2016 eugene golovanov. All rights reserved.
//

import UIKit

class JoinVC: UIViewController {

    
    //---------------------------------------------------------------------------
    // MARK: - Properties
    
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var chatroomField: UITextField!
    
    
    
    //---------------------------------------------------------------------------
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    
    
    
    //---------------------------------------------------------------------------
    // MARK: - Actions

    
    @IBAction func joinChatAction(_ sender: UIButton) {
        SocketIOManager.sharedInstance.establishConnection()

        performSegue(withIdentifier: "chatSegue", sender: self)
    }
    
    
    
    
    //---------------------------------------------------------------------------
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chatSegue" {
            let navVC = segue.destination as! UINavigationController
            let vc = navVC.viewControllers.first as! ChatVC
            vc.nameLabel.text = self.usernameField.text!
            vc.roomLabel.text = self.chatroomField.text!
        }
    }


}
