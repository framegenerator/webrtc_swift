//
//  AppDelegate.swift
//  Chatroom
//
//  Created by eugene golovanov on 7/25/16.
//  Copyright © 2016 eugene golovanov. All rights reserved.
//

import UIKit
import WebRTC

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        RTCPeerConnectionFactory.initializeSSL()
        let fieldTrials = [kRTCFieldTrialH264HighProfileKey: kRTCFieldTrialEnabledValue]
        RTCInitFieldTrialDictionary(fieldTrials)
        RTCInitializeSSL()
        RTCSetupInternalTracer()
        #if NDEBUG
            // In debug builds the default level is LS_INFO and in non-debug builds it is
            // disabled. Continue to log to console in non-debug builds, but only
            // warnings and errors.
            RTCSetMinDebugLogLevel(RTCLoggingSeverityWarning)
        #endif
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        SocketIOManager.sharedInstance.closeConnection()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        SocketIOManager.sharedInstance.establishConnection()
    }

    func applicationWillTerminate(_ application: UIApplication) {
//        RTCPeerConnectionFactory.deinitializeSSL()
        RTCShutdownInternalTracer()
        RTCCleanupSSL()

    }


}

