//
//  ARDCaptureController.swift
//  Chatroom
//
//  Created by Golovanov, Eugene on 8/15/17.
//  Copyright © 2017 eugene golovanov. All rights reserved.
//

import Foundation
import WebRTC

// Controls the camera. Handles starting the capture, switching cameras etc.
final class ARDCaptureController {
    private var capturer: RTCCameraVideoCapturer
    private var usingFrontCamera: Bool = true
    
    init(capturer:RTCCameraVideoCapturer) {
        self.capturer = capturer
    }
    
    func startCapture() {
        let position: AVCaptureDevicePosition = usingFrontCamera ? .front : .back
        let device = findDevice(for: position)
        let format = selectFormat(for: device)
        let fps: Int = selectFps(for: format)
        capturer.startCapture(with: device, format: format, fps: fps)
    }
    
    func stopCapture() {
        capturer.stopCapture()
    }
    
    func switchCamera() {
        usingFrontCamera = !usingFrontCamera
        startCapture()
    }
    
    // MARK: - Private
    func findDevice(for position: AVCaptureDevicePosition) -> AVCaptureDevice {
        let captureDevices: [AVCaptureDevice] = RTCCameraVideoCapturer.captureDevices()
        for device: AVCaptureDevice in captureDevices {
            if device.position == position {
                return device
            }
        }
        return captureDevices[0]
    }
    
    func selectFormat(for device: AVCaptureDevice) -> AVCaptureDeviceFormat {
        let formats: [AVCaptureDeviceFormat] = RTCCameraVideoCapturer.supportedFormats(for: device)
        let targetWidth = Int(640)
        let targetHeight = Int(480)
        var selectedFormat: AVCaptureDeviceFormat? = nil
        var currentDiff: Int = Int.max
        for format: AVCaptureDeviceFormat in formats {
            let dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription)
            let diff = abs(targetWidth - Int(dimension.width)) + abs(targetHeight - Int(dimension.height))
            if diff < currentDiff {
                selectedFormat = format
                currentDiff = diff
            }
        }
        assert(selectedFormat != nil, "No suitable capture format found.")
        return selectedFormat!
    }
    
    func selectFps(for format: AVCaptureDeviceFormat) -> Int {
        var maxFramerate: Float64 = 0
        for fpsRange: AVFrameRateRange in (format.videoSupportedFrameRateRanges as! [AVFrameRateRange]) {
            maxFramerate = fmax(maxFramerate, fpsRange.maxFrameRate)
        }
        return Int(maxFramerate)
    }
}
