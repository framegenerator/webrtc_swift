//
//  VideoCallView.swift
//  Chatroom
//
//  Created by Golovanov, Eugene on 8/17/17.
//  Copyright © 2017 eugene golovanov. All rights reserved.
//

import UIKit
import AVFoundation
import WebRTC


private let kButtonPadding: CGFloat = 16
private let kButtonSize: CGFloat = 48
private let kLocalVideoViewSize: CGFloat = 120
private let kLocalVideoViewPadding: CGFloat = 8
private let kStatusBarHeight: CGFloat = 20

protocol VideoCallViewDelegate: NSObjectProtocol {
    // Called when the camera switch button is pressed.
    func videoCallViewDidSwitchCamera(_ view: VideoCallView)
    
    // Called when the route change button is pressed.
    func videoCallViewDidChangeRoute(_ view: VideoCallView)
    
    // Called when the hangup button is pressed.
    func videoCallViewDidHangup(_ view: VideoCallView)
}

// Video call view that shows local and remote video, provides a label to
// display status, and also a hangup button.
class VideoCallView: UIView, RTCEAGLVideoViewDelegate {
    //    private(set) var statusLabel: UILabel
    private(set) var localVideoView: RTCCameraPreviewView
    private(set) var remoteVideoView: RTCEAGLVideoView
    weak var delegate: VideoCallViewDelegate?
    
    private var routeChangeButton: UIButton
    private var cameraSwitchButton: UIButton
    private var hangupButton: UIButton
    private var remoteVideoSize = CGSize.zero
    
    override init(frame: CGRect) {
        let remoteView = RTCEAGLVideoView(frame: CGRect.zero)
        remoteVideoView = remoteView
        
        
        localVideoView = RTCCameraPreviewView(frame: CGRect.zero)
        
        
        routeChangeButton = UIButton(type: .custom)
        routeChangeButton.backgroundColor = UIColor.white
        routeChangeButton.layer.cornerRadius = kButtonSize / 2
        routeChangeButton.layer.masksToBounds = true
        
        var image = UIImage(named: "ic_surround_sound_black_24dp.png")
        routeChangeButton.setImage(image, for: .normal)
        
        
        // TODO(tkchin): don't display this if we can't actually do camera switch.
        cameraSwitchButton = UIButton(type: .custom)
        cameraSwitchButton.backgroundColor = UIColor.white
        cameraSwitchButton.layer.cornerRadius = kButtonSize / 2
        cameraSwitchButton.layer.masksToBounds = true
        image = UIImage(named: "ic_switch_video_black_24dp.png")
        cameraSwitchButton.setImage(image, for: .normal)
        
        
        hangupButton = UIButton(type: .custom)
        hangupButton.backgroundColor = UIColor.red
        hangupButton.layer.cornerRadius = kButtonSize / 2
        hangupButton.layer.masksToBounds = true
        //        image = UIImage(for: "ic_call_end_black_24dp.png", color: UIColor.white)
        image = UIImage(named: "ic_call_end_black_24dp.png")
        hangupButton.setImage(image, for: .normal)
        
        
        //        statusLabel = UILabel(frame: CGRect.zero)
        //        statusLabel.font = UIFont(name: "Roboto", size: CGFloat(16))
        //        statusLabel.textColor = UIColor.white
        
        super.init(frame: frame)
        
        remoteView.delegate = self
        addSubview(remoteVideoView)
        
        addSubview(localVideoView)
        
        routeChangeButton.addTarget(self, action: #selector(self.onRouteChange), for: .touchUpInside)
        addSubview(routeChangeButton)
        
        cameraSwitchButton.addTarget(self, action: #selector(self.onCameraSwitch), for: .touchUpInside)
        addSubview(cameraSwitchButton)
        
        hangupButton.addTarget(self, action: #selector(self.onHangup), for: .touchUpInside)
        addSubview(hangupButton)
        
        //        addSubview(statusLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        let bounds: CGRect = self.bounds
        if remoteVideoSize.width > 0 && remoteVideoSize.height > 0 {
            // Aspect fill remote video into bounds.
            
            var remoteVideoFrame: CGRect = AVMakeRect(aspectRatio: remoteVideoSize, insideRect: bounds)
            var scale: CGFloat = 1
            if remoteVideoFrame.size.width > remoteVideoFrame.size.height {
                // Scale by height.
                scale = bounds.size.height / remoteVideoFrame.size.height
            }
            else {
                // Scale by width.
                scale = bounds.size.width / remoteVideoFrame.size.width
            }
            remoteVideoFrame.size.height *= scale
            remoteVideoFrame.size.width *= scale
            remoteVideoView.frame = remoteVideoFrame
            remoteVideoView.center = CGPoint(x: CGFloat(bounds.midX), y: CGFloat(bounds.midY))
        }
        else {
            remoteVideoView.frame = bounds
        }
        // Aspect fit local video view into a square box.
        var localVideoFrame = CGRect(x: CGFloat(0), y: CGFloat(0), width: kLocalVideoViewSize, height: kLocalVideoViewSize)
        // Place the view in the bottom right.
        localVideoFrame.origin.x = bounds.maxX - localVideoFrame.size.width - kLocalVideoViewPadding
        localVideoFrame.origin.y = bounds.maxY - localVideoFrame.size.height - kLocalVideoViewPadding
        
        localVideoView.frame = localVideoFrame
        
        // Place hangup button in the bottom left.
        hangupButton.frame = CGRect(x: CGFloat(bounds.minX + kButtonPadding), y: CGFloat(bounds.maxY - kButtonPadding - kButtonSize), width: kButtonSize, height: kButtonSize)
        
        // Place button to the right of hangup button.
        var cameraSwitchFrame: CGRect = hangupButton.frame
        cameraSwitchFrame.origin.x = cameraSwitchFrame.maxX + kButtonPadding
        cameraSwitchButton.frame = cameraSwitchFrame
        // Place route button to the right of camera button.
        var routeChangeFrame: CGRect = cameraSwitchButton.frame
        routeChangeFrame.origin.x = routeChangeFrame.maxX + kButtonPadding
        routeChangeButton.frame = routeChangeFrame
        //        statusLabel.sizeToFit()
        //        statusLabel.center = CGPoint(x: CGFloat(bounds.midX), y: CGFloat(bounds.midY))
    }
    
    // MARK: - RTCEAGLVideoViewDelegate
    func videoView(_ videoView: RTCEAGLVideoView, didChangeVideoSize size: CGSize) {
        if videoView == remoteVideoView {
            remoteVideoSize = size
        }
        setNeedsLayout()
    }
    
    // MARK: - Private
    func onCameraSwitch(_ sender: Any) {
        delegate?.videoCallViewDidSwitchCamera(self)
    }
    
    func onRouteChange(_ sender: Any) {
        delegate?.videoCallViewDidChangeRoute(self)
    }
    
    func onHangup(_ sender: Any) {
        delegate?.videoCallViewDidHangup(self)
    }
}





