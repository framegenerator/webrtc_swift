//
//  AppClient.swift
//  Chatroom
//
//  Created by Golovanov, Eugene on 8/3/17.
//  Copyright © 2017 eugene golovanov. All rights reserved.
//



/*
 --[SOMETHING]--> represents a message of type "Something" sent from the caller to the callee
<--[SOMETHING]-- represents a message of type "Something" sent from the callee to the caller
 SS: Message sent via Signal Service
 
 +---------------------------------------------------------------------+--------------------------------------------------------+
 |                         Caller                                      |                           Callee                       |
 +---------------------------------------------------------------------+--------------------------------------------------------+
 Creates an RTCPeerConnection object
 Create Offer SDP [RTCPeerConnection.offer()]
 [RTCPeerConnection.setLocalDescription()] with his SDP Offer.
 Send offer SDP to Callee                        --[SS.CallOffer]-->
                                                                         Receives SDP Offer, calls [RTCPeerConnection.setRemoteDescription()] with Offer, so that his RTCPeerConnection knows about Caller's setup
                                                                         Creates Answer SDP with [RTCPeerConnection.answer()]
                                                                         Set Anwer SDP as local with [RTCPeerConnection.setLocalDescription()]
                                                                         <--[SS.CallAnswer]--      Send Anwer SDP to Caller
 Receives SDP Answer, calls [RTCPeerConnection.setRemoteDescription()]
 with this Anwer
 
 ----------------------------------------------------------------------------------------------------------------------------------
 Ice:
 
 When Create Offer SDP created ICE candidates are
 generated in [didGenerate candidate]
 Send Ice Candidates with signal service         --[SS.CallOffer]-->
                                                                         Receives Ice Candidates and add it to the [RTCPeerConnection.add()]
 */

import UIKit
import AVFoundation
import WebRTC

enum AppClientState : Int {
    // Disconnected from servers.
    case disconnected
    // Connecting to servers.
    case connecting
    // Connected to servers.
    case connected
}

protocol AppClientDelegate {
    func appClient(client:AppClient, didReceiveLocal videoTrack:RTCVideoTrack)
    func appClient(client:AppClient, didReceiveRemote videoTrack:RTCVideoTrack)
    
    func appClient(_ client: AppClient, didCreateLocalCapturer localCapturer: RTCCameraVideoCapturer)
    func appClient(_ client: AppClient, didError error: Error?)
    
    func appClient(_ client: AppClient, didChange state: AppClientState)
    
}

class AppClient: NSObject {
    
    var peerConnection: RTCPeerConnection!
    var factory: RTCPeerConnectionFactory!
    var iceServers = [RTCIceServer]()
    var delegate:AppClientDelegate?
    
    private(set) var state:AppClientState = .disconnected
    
    private var localVideoTrack: RTCVideoTrack!
    
    var configuration = RTCConfiguration() //Added for Webrtc
    var isInitiator = false
    
    
    init(delegate:AppClientDelegate) {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppClient.webrtcLocalAnswerReceived), name: NSNotification.Name(rawValue: "webrtcLocalAnswerReceived"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppClient.webrtcCandidateReceived), name: NSNotification.Name(rawValue: "webrtcCandidateReceived"), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(AppClient.byeReceived), name: NSNotification.Name(rawValue: "webrtcBye"), object: nil)
        
        
        self.delegate = delegate
        self.factory = RTCPeerConnectionFactory()
        self.iceServers = [self.defaultSTUNServer()]
        self.configuration.iceServers = self.iceServers
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        self.disconnect()
    }
    
    
    //MARK: - Connect/Disconnect
    
    
    func startSignalingIfReady() {
        
        self.setState(.connecting)
        
        //Create peer connection
        let constraints = self.defaultPeerConnectionConstraints()
        self.peerConnection = self.factory.peerConnection(with: self.configuration, constraints: constraints, delegate: self)
        
        self.createAudioSender()
        self.createVideoSender()
        
        if self.isInitiator {
            self.sendOffer()
        } else {
            self.doAnswer()
        }
    }
    
    
    func disconnect() {
        if state == AppClientState.disconnected {
            return
        }
        self.isInitiator = false
        self.localVideoTrack = nil
        self.factory.stopAecDump()
        self.peerConnection.stopRtcEventLog()
        self.peerConnection = nil
        self.state = .disconnected
    }
    
    
    func setState(_ state: AppClientState) {
        if self.state == state {
            return
        }
        self.state = state
        self.delegate?.appClient(self, didChange: self.state)
    }
    
    /*
     Hangup from appclient
     */
    func byeReceived() {
        self.setState(.disconnected)
    }
    
    //MARK: - Media Senders
    
    func createAudioSender() {
        let constraints = defaultMediaAudioConstraints()
        let source = factory.audioSource(with: constraints)
        let track = factory.audioTrack(with: source, trackId: "ARDAMSa0")
        let sender = peerConnection.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: "ARDAMS")
        sender.track = track
    }
    
    func createVideoSender() {
        let sender = peerConnection.sender(withKind: kRTCMediaStreamTrackKindVideo, streamId: "ARDAMS")
        self.localVideoTrack = self.createLocalVideoTrack()
        sender.track = self.localVideoTrack
        delegate?.appClient(client: self, didReceiveLocal: localVideoTrack)
    }
    
    
    func createLocalVideoTrack() -> RTCVideoTrack {
        let source: RTCVideoSource = factory.videoSource()
        let capturer = RTCCameraVideoCapturer(delegate: source)
        delegate?.appClient(self, didCreateLocalCapturer: capturer)
        let localVideoTrack = factory.videoTrack(with: source, trackId: "ARDAMSv0")
        
        return localVideoTrack
    }
    
    //---------------------------------------------------------------------------------------------------------
    //MARK: - notification
    
    func webrtcLocalAnswerReceived(_ notification: Notification) {
        if let userInfo = (notification as NSNotification).userInfo, let type = userInfo["type"] as? String, let sdp = userInfo["sdp"] as? String {
            print("ANSWER TYPE: \(type)")
            print("ANSWER SDP: \(sdp)")
            let sdpType = RTCSdpType.answer
            let remoteDesc = RTCSessionDescription(type: sdpType, sdp: sdp)
            self.peerConnection?.setRemoteDescription(remoteDesc, completionHandler: { [weak self] (error) in
                print("-->> setRemoteDescription")
            })
        }
        
    }
    
    func webrtcCandidateReceived(_ notification: Notification) {
        if let userInfo = (notification as NSNotification).userInfo,let candidate = userInfo["candidate"] as? String, let label = userInfo["label"] as? NSInteger, let id = userInfo["id"] as? String  {
            /*
             { type: 'candidate',
             label: 0,
             id: 'video',
             candidate: 'candidate:2090411078 1 udp 2122194687 169.254.167.230 64024 typ host generation 0 ufrag QHcl network-id 3' }
             */
            
            let candidate = RTCIceCandidate(sdp: candidate, sdpMLineIndex: Int32(label), sdpMid: id)
            self.peerConnection?.add(candidate)
        }
    }
    
    //---------------------------------------------------------------------------------------------------------
    //MARK: - Defaults
    
    func defaultSTUNServer() -> RTCIceServer {
        return RTCIceServer.init(urlStrings: ["stun:stun.l.google.com:19302"])
    }
    
    func defaultPeerConnectionConstraints() -> RTCMediaConstraints {
        let optionalConstraints = ["DtlsSrtpKeyAgreement": "true"];
        let constraints = RTCMediaConstraints.init(mandatoryConstraints: nil, optionalConstraints: optionalConstraints)
        return constraints
    }
    
    func defaultMediaStreamConstraints() -> RTCMediaConstraints {
        return RTCMediaConstraints.init(mandatoryConstraints: nil, optionalConstraints: nil)
    }
    
    func defaultMediaAudioConstraints() -> RTCMediaConstraints {
        let valueLevelControl: String = kRTCMediaConstraintsValueFalse
        let mandatoryConstraints = [kRTCMediaConstraintsLevelControl: valueLevelControl]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constraints
    }
    
    func defaultOfferConstraints() -> RTCMediaConstraints {
        let mandatoryConstraints = [
            "OfferToReceiveVideo" : "true",
            "OfferToReceiveAudio" : "true"
        ]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constraints
    }
    
    //---------------------------------------------------------------------------------------------------------
    //MARK: - Offer/Answer
    
    func sendOffer() {
        print("-> sendOffer")
        self.peerConnection?.offer(for: self.defaultOfferConstraints(), completionHandler: {[weak self] (sdp, error) in
            self?.peerConnection(self?.peerConnection, didCreateSessionDescription: sdp, error: error)
        })
    }
    
    func doAnswer() {
        print("-> Answering to Offer")
        if let remoteSDP = WebRTCManager.shared.remoteSDP {
            
            let sdpType = RTCSdpType.offer
            let remoteDesc = RTCSessionDescription(type: sdpType, sdp: remoteSDP)
            self.peerConnection?.setRemoteDescription(remoteDesc, completionHandler: { [weak self] (error) in
                print("-->> setRemoteDescription Done")
                
                self?.peerConnection?.answer(for: (self?.defaultOfferConstraints())!, completionHandler: { (sdp, error) in
                    self?.peerConnection(self?.peerConnection, didCreateSessionDescription: sdp, error: error)
                })
            })
        }
        
    }
    
    
    // ----------------------------------------------------
    // MARK: - RTCSessionDescriptionDelegate
    
    func peerConnection(_ peerConnection: RTCPeerConnection!, didCreateSessionDescription sdp: RTCSessionDescription!, error: Error!) {
        
        print("--> didCreateSessionDescription Session Description)")
        
        // Prefer codec from settings if available.
        let sdpPreferringCodec: RTCSessionDescription = ARDSDPUtils.description(for: sdp, preferredVideoCodec: "h264")
        //            return @[ @"H264", @"VP8", @"VP9" ];
        
        self.peerConnection?.setLocalDescription(sdpPreferringCodec, completionHandler: {[weak self] (error) in
            if error == nil {
                
                if let sSelf = self {
                    // Send offer/answer through the signaling channel of our application
                    if sSelf.isInitiator {
                        print("\nSENDING Offer with Socket.IO: \n\(sdpPreferringCodec.sdp)")
                        SocketIOManager.sharedInstance.sendLocalDescriptionOffer(localDescription: sdpPreferringCodec.sdp)
                    } else {
                        print("\nSENDING Answer with Socket.IO: \n\(sdpPreferringCodec.sdp)")
                        SocketIOManager.sharedInstance.sendLocalDescriptionAnswer(localDescription: sdpPreferringCodec.sdp)
                    }
                }
                
            } else {
                print("Error with didCreateSessionDescription: \(String(describing: error))")
            }
        })
    }
    
}

extension AppClient: RTCVideoCapturerDelegate {
    public func capturer(_ capturer: RTCVideoCapturer, didCapture frame: RTCVideoFrame) {
        
    }
    
}

extension AppClient: RTCPeerConnectionDelegate {
    
    // ----------------------------------------------------
    // MARK: - RTCPeerConnectionDelegate
    
    
    /** Called when the SignalingState changed. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        print("--->didChange stateChanged: \(stateChanged)")
    }
    
    
    /** Called when media is received on a new stream from remote peer. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        print("--->didAdd stream: \(stream)")
        if let remtVideoTrack = stream.videoTracks.first {
            delegate?.appClient(client: self, didReceiveRemote: remtVideoTrack)
            self.setState(.connected)
        }
    }
    
    
    /** Called when a remote peer closes a stream. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        print("--->didRemove stream: \(stream)")
        
    }
    
    
    /** Called when negotiation is needed, for example ICE has restarted. */
    public func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        print("--->peerConnectionShouldNegotiate: \(peerConnection)")
        
    }
    
    
    /** Called any time the IceConnectionState changes. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        print("--->didChange newState: \(newState.rawValue)")
        
    }
    
    
    /** Called any time the IceGatheringState changes. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        print("--->didChange newState: \(newState.rawValue)")
        
    }
    
    
    /** New ice candidate has been found. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        print("--->didGenerate candidate: \(candidate)")
        
        /*
         { type: 'candidate',
         label: 0,
         id: 'video',
         candidate: 'candidate:2090411078 1 udp 2122194687 169.254.167.230 64024 typ host generation 0 ufrag QHcl network-id 3' }
         */
        if let sdpMid = candidate.sdpMid {
            let payload = ["type": SignalingMessageType.candidate.rawValue,
                           "label": candidate.sdpMLineIndex,
                           "id": sdpMid,
                           "candidate" : candidate.sdp]
                as [String : AnyObject]
            
            SocketIOManager.sharedInstance.sendIceCanditate(payload: payload)
        }
    }
    
    /** Called when a group of local Ice candidates have been removed. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        print("--->didRemove candidates: \(candidates)")
        
    }
    
    /** New data channel has been opened. */
    public func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        print("--->didOpen dataChannel: \(dataChannel)")
        
    }
    
    
}
