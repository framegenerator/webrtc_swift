//
//  ARDSDPUtils.swift
//  Chatroom
//
//  Created by Golovanov, Eugene on 8/15/17.
//  Copyright © 2017 eugene golovanov. All rights reserved.
//

import Foundation
import WebRTC

class ARDSDPUtils: NSObject {
    
    // Updates the original SDP description to instead prefer the specified video
    // codec. We do this by placing the specified codec at the beginning of the
    // codec list if it exists in the sdp.
    class func description(for description: RTCSessionDescription, preferredVideoCodec codec: String) -> RTCSessionDescription {
        let sdpString: String = description.sdp
        let lineSeparator: String = "\n"
        let mLineSeparator: String = " "
        
        // Copied from PeerConnectionClient.java.
        // TODO(tkchin): Move this to a shared C++ file.
        var lines: [String] = sdpString.components(separatedBy: lineSeparator)
        
        // Find the line starting with "m=video".
        var mLineIndex: Int = -1
        for i in 0..<lines.count {
            if lines[i].hasPrefix("m=video") {
                mLineIndex = i
                break
            }
        }
        if mLineIndex == -1 {
            print("No m=video line, so can't prefer %@", codec)
            return description
        }
        
        // An array with all payload types with name |codec|. The payload types are
        // integers in the range 96-127, but they are stored as strings here.
        var codecPayloadTypes = [String]()
        // a=rtpmap:<payload type> <encoding name>/<clock rate>
        // [/<encoding parameters>]
        let pattern: String = "^a=rtpmap:(\\d+) \(codec)(/\\d+)+[\r]?$"
        let regex = try? NSRegularExpression(pattern: pattern, options: [])
        for line: String in lines {
            let codecMatches: NSTextCheckingResult? = regex?.firstMatch(in: line, options: [], range: NSRange(location: 0, length: line.characters.count))
            if let codecM = codecMatches {
                let append = (line as NSString).substring(with: codecM.rangeAt(1))
                codecPayloadTypes.append(append)
            }
        }
        if codecPayloadTypes.count == 0 {
            print("No payload types with name %@", codec)
            return description
        }
        let origMLineParts: [String] = lines[mLineIndex].components(separatedBy: mLineSeparator)
        // The format of ML should be: m=<media> <port> <proto> <fmt> ...
        let kHeaderLength = 3
        if origMLineParts.count <= kHeaderLength {
            print("Wrong SDP media description format: %@", lines[mLineIndex])
            return description
        }
        // Split the line into header and payloadTypes.
        let headerRange = NSRange(location: 0, length: kHeaderLength)
        let payloadRange = NSRange(location: kHeaderLength, length: origMLineParts.count - kHeaderLength)
        
        
        let header: [String] = (origMLineParts as NSArray).subarray(with: headerRange) as! [String]
        var payloadTypes: [String] = (origMLineParts as NSArray).subarray(with: payloadRange) as! [String]
        // Reconstruct the line with |codecPayloadTypes| moved to the beginning of the
        // payload types.
        var newMLineParts: [String] = NSMutableArray.init(capacity: origMLineParts.count) as! [String]
        newMLineParts.append(contentsOf: header)
        newMLineParts.append(contentsOf: codecPayloadTypes)
        (payloadTypes as! NSMutableArray).removeObjects(in: codecPayloadTypes)
        
        newMLineParts.append(contentsOf: payloadTypes)
        
        let newMLine: String = (newMLineParts as NSArray).componentsJoined(by: mLineSeparator)
        lines[mLineIndex] = newMLine
        
        
        let mangledSdpString: String = (lines as NSArray).componentsJoined(by: lineSeparator)
        
        return RTCSessionDescription(type: description.type, sdp: mangledSdpString)
    }
}
