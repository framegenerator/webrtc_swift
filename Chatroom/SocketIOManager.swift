//
//  SocketIOManager.swift
//  Chatroom
//
//  Created by eugene golovanov on 7/26/16.
//  Copyright © 2016 eugene golovanov. All rights reserved.
//

import Foundation
import SocketIO

enum SignalingMessageType : String {
    case offer = "offer"
    case answer = "answer"
    case candidate = "candidate"
    case bye = "bye"
}

class SocketIOManager: NSObject {
    
    //----------------------------------------------------------------------------------------
    //MARK: - Properties
    
    static let sharedInstance = SocketIOManager()
    
    //    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://localhost:3000")! as URL)
//    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "https://webrtc-chat-yujin.herokuapp.com/")! as URL)
//    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "https://webrtc-textor-playground.herokuapp.com/")! as URL)
        var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://192.168.0.24:3000")! as URL) // HOME
//        var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://172.20.10.4:3000")! as URL) // iPhone yujin network
//        var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://192.168.43.227:3000")! as URL) // Galaxy s6 aus  network

    //----------------------------------------------------------------------------------------
    //MARK: - Init
    
    
    override init() {
        super.init()
    }
    
    //    func listenSocket() {
    //        socket.on("connect") {data, ack in
    //            print("socket connected")
    //        }
    //    }
    
    //----------------------------------------------------------------------------------------
    //MARK: - Connection
    
    func establishConnection() {
        socket.connect()
    }
    
    
    func closeConnection() {
        socket.disconnect()
    }
    
    //    func recconnectConnection() {
    //        socket.reconnect()
    //    }
    
    
    func connectToChatroomWithNickname(chatroom:String, nickname: String) {
        self.addHandlers()
        socket.emit("joinRoom", ["name": nickname,"room": chatroom])
    }
    
    //----------------------------------------------------------------------------------------
    //MARK: - Messages
    
    func sendMessage(message:String, name:String, timestamp:Int) {
        socket.emit("message", ["text": message, "name": name, "timestamp": timestamp])
    }
    
    
    func getChatMessage(_ completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("message") { (dataArray, socketAck) -> Void in
            
            guard let data = dataArray.first as? [String : AnyObject] else {
                print("something wrong with message data")
                return
            }
            
            var messageDictionary = [String: AnyObject]()
            messageDictionary["name"] = data["name"]
            messageDictionary["text"] = data["text"]
            messageDictionary["timestamp"] = data["timestamp"]
            completionHandler(messageDictionary)
        }
    }
    
    //----------------------------------------------------------------------------------------
    //MARK: - WebRTC Stuff
    
    
    func addHandlers() {
        self.socket.on("error") {data in
            print("bliat socket ERROR")
            print(data)
        }
        
        self.socket.on("ready") { (data, socketAck) -> Void in
            print("\nReady to Call, Two sockets in same room\n")
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "showCallButton"), object: nil, userInfo: nil))
        }
        
        self.socket.on("webrtcMessage") { (data, socketAck) -> Void in
            
            if let bye = data.first as? String, bye == SignalingMessageType.bye.rawValue {
                print("\n\n---------------webrtcMessage: +++ BYE +++------------------------")
                print("BYE BYE BYE")
                print("--------------------------------------------------------------------\n\n")
                if let client = WebRTCManager.shared.appClient {
                    client.byeReceived()
                }
                return
            }
        
            guard let d = data.first as? [String : AnyObject], let type = d["type"] as? String else {
                print("something wrong with webrtcMessage data")
                return
            }
            
            switch type {
            case SignalingMessageType.answer.rawValue:
                
                guard let sdp = d["sdp"] as? String else { print("\nAnswer SDP Error\n"); return }
                print("\n\n---------------webrtcMessage: +++ ANSWER +++------------------------")
                let userInfo = ["type" : type, "sdp" : sdp]
                print("WebRTC ANSWER userinfo ===: \(userInfo)")
                print("--------------------------------------------------------------------\n\n")
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "webrtcLocalAnswerReceived"), object: nil, userInfo: userInfo))
                
                break
            case SignalingMessageType.offer.rawValue:
                
                guard let sdp = d["sdp"] as? String else { print("\nOffer SDP Error\n"); return }
                let userInfo = ["type" : type, "sdp" : sdp]
                print("\n\n---------------webrtcMessage: +++ OFFER +++------------------------")
                print("WebRTC Offer userinfo ===: \(userInfo)")
                print("--------------------------------------------------------------------\n\n")

                if WebRTCManager.shared.remoteSDP == nil {
                    NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "webrtcOfferReceived"), object: nil, userInfo: userInfo))
                }
                
                break
            case SignalingMessageType.candidate.rawValue:
                /*
                 { type: 'candidate',
                 label: 0,
                 id: 'video',
                 candidate: 'candidate:2090411078 1 udp 2122194687 169.254.167.230 64024 typ host generation 0 ufrag QHcl network-id 3' }
                 */
                guard let candidate = d["candidate"] as? String,
                    let id = d["id"],
                    let label = d["label"] else { print("\nSDP Error\n"); return }
                
                print("\n\n---------------webrtcMessage: +++ CANDIDATE +++------------------------")
                let userInfo = ["type": SignalingMessageType.candidate.rawValue,
                                "label": label,
                                "id": id,
                                "candidate" : candidate]
                    as [String : AnyObject]
                print("WebRTC CANDIDATE ===: \(userInfo)")
                print("--------------------------------------------------------------------\n\n")
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "webrtcCandidateReceived"), object: nil, userInfo: userInfo))
                break
            default:
                break
            }
            
        }
    }
    
    func sendLocalDescriptionOffer(localDescription:String) {
        let payload = ["type": SignalingMessageType.offer.rawValue, "sdp": localDescription]
        socket.emit("webrtcMessage", payload)
    }
    
    func sendLocalDescriptionAnswer(localDescription:String) {
        let payload = ["type": SignalingMessageType.answer.rawValue, "sdp": localDescription]
        socket.emit("webrtcMessage", payload)
    }

    func sendIceCanditate(payload: [String : AnyObject]) {
        socket.emit("webrtcMessage", payload)
    }

    func sendBye() {
        socket.emit("webrtcMessage", "bye")
    }
    
}


